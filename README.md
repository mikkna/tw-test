# Transferwise test task

## Install npm and Node if not installed
* [https://nodejs.org/en/](https://nodejs.org/en/)

## Run app
* assumes that ports 3090 & 3000 are available
* `npm install`
* `npm run build`
* `npm run start`
* navigate to [http://localhost:3090/](http://localhost:3090/) 

## Run tests
* `npm run test:api` to test backend
* `npm run test:web` to test frontend

You can generate new contacts by running `npm run generate n`, where `n` represents the number of contacts to create.




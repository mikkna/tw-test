let express = require('express');

let app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

//Middleware
app.use(require('./middleware/headers'));
app.use(require('./middleware/register-services'));

// Router
app.use('/api/v1', require('./router'));

module.exports = app;

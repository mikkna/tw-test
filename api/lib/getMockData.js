const fs = require('fs');
const path = require('path');
const faker = require('faker');

const MOCK_DB_PATH = "../db";
const MOCK_DB_NAME = "contacts.json";

const count = process.argv[2] || 10;

const images = [
  'https://images.pexels.com/photos/617278/pexels-photo-617278.jpeg?cs=srgb&dl=adorable-animal-blur-617278.jpg&fm=jpg',
  'https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
  'http://www.creditlenders.info/wp-content/uploads/stock-photo-guy-for-science-please.jpg',
  'https://media.istockphoto.com/photos/happy-young-woman-smiling-portrait-picture-id485734753',
  'https://previews.123rf.com/images/luckybusiness/luckybusiness1006/luckybusiness100600168/7204864-young-dangerous-woman-holding-one-gold-fish-and-gun.jpg',
];


function getContacts() {
  let res = [];

  for (let i = 0; i < count; i++) {
    res.push(getNewContact())
  }

  return res;
}

function getNewContact() {
  return {
    id: Math.floor(Math.random() * 1000000) + 1,
    name: faker.name.findName(),
    age: Math.floor(Math.random() * 100) + 1,
    phoneNumber: faker.phone.phoneNumber(),
    address: faker.address.streetAddress(),
    imageURL: images[Math.floor(Math.random()*images.length)]
  }
}

fs.writeFileSync(path.join(__dirname, MOCK_DB_PATH, MOCK_DB_NAME), JSON.stringify(getContacts()));

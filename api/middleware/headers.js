module.exports = function (req, res, next) {
  // would also set Cache-Control headers in a real app
  res.header("Access-Control-Allow-Origin", "*");
  next();
};
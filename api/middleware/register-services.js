/**
 * Provide services controllers
 * @param req
 * @param res
 * @param next
 */
module.exports = function (req, res, next) {
  res.services = {
    contact: new (require('../services/contact'))(require('../db/contacts')),
  };
  next();
};
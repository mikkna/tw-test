const express = require('express');
const router = express.Router();

router.get('/contacts', require('./routes/contacts').getContacts);

module.exports = router;

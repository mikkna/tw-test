module.exports.getContacts = function (req, res) {
  let contacts = res.services.contact.get(req.query);
  return res.json(contacts);
};
class ContactService {
  constructor(contacts) {
    // This would be a DB model in a real app
    this.contacts = contacts;
  }

  /**
   *
   * @param {Number|String} limit
   * @param {Number|String} offset
   * @param {String?} query
   * @returns {Array}
   */
  get({limit = 10, offset = 0, query = ''}) {
    limit = parseInt(limit);
    offset = parseInt(offset);

    const words = query.trim().toLowerCase().split(' ');

    return this.contacts
      .filter(contact => {
        // since it's not an actual DB query, I can easily implement a text-based search with some clever JS magic
        // otherwise some selected fields would need to be indexed
        return !query || ContactService.stringContainsWords(JSON.stringify(contact).toLowerCase(), words);
      })
      // this could be optimized to return when the limit is reached
      .slice(offset, limit + offset);
  }

  /**
   *
   * @param {String} string
   * @param {Array} words
   * @returns {boolean}
   */
  static stringContainsWords(string, words) {
    return words.filter(word => string.includes(word)).length === words.length;
  }
}

module.exports = ContactService;
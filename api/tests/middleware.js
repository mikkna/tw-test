let chai = require('chai');
let assert = chai.assert;

describe('middleware', () => {

  describe('register-services', () => {
    it('has registered services.contact correctly', done => {
      let res = {};

      require('../middleware/register-services')(null, res, () => {});
      let { services } = res;
      assert.isDefined(services.contact);
      done();
    })
  })

});
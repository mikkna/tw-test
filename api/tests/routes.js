let chai = require('chai');
let assert = chai.assert;

describe('routes', () => {

  describe('/contacts', () => {

    it('responds as expected, calls res.json', done => {
      const routeAction = require('../routes/contacts').getContacts;

      // service returns the arguments given
      const contactService = {
        get(params) {
          return params;
        }
      };
      const res = {
        services: {contact: contactService},
        json(data) {
          return data;
        }
      };

      assert.deepEqual(
        routeAction({query: {}}, res), {});

      let query = {limit: "2", offset: "12", query: "test"};
      assert.equal(
        routeAction({query}, res),
        query
      );
      done();


    });

  });

});
let chai = require('chai');
let assert = chai.assert;

const ContactService = require('../services/contact');

describe('services', () => {

  describe('contact', () => {

    it('can create the service', done => {
      const contacts = [];
      const contactService = new ContactService(contacts);

      assert.isObject(contactService);
      assert.equal(contacts, contactService.contacts);
      done();
    });

    it('has required methods', done => {
      const contactService = new ContactService();

      assert.isFunction(contactService.get);

      done();
    });

    it('get works like a charm', done => {
      const searchable = {text: 'test searchable test'};
      const contacts = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, searchable];
      const contactService = new ContactService(contacts);

      assert.deepEqual(contactService.get({limit: 2}), [0, 1]);
      assert.deepEqual(contactService.get({limit: 1, offset: 3}), [3]);
      assert.deepEqual(contactService.get({limit: 1, offset: 3}), [3]);
      assert.equal(contactService.get({}).length, 10);
      assert.deepEqual(contactService.get({query: "search"}), [searchable]);
      assert.deepEqual(contactService.get({query: "seARch"}), [searchable]);
      assert.isEmpty(contactService.get({query: 'this doesn\'t exist'}));
      done();
    });

    it('stringContainsWords works like a charm', () => {
      let string = JSON.stringify({
        id: 1,
        field1: 'word1 word2',
        field2: 'word3 word4',
      });
      let words1 = ['word1', 'word2'],
        words2 = ['word1', 'word4'],
        words3 = ['word5', 'word6'];

      assert.isTrue(ContactService.stringContainsWords(string, words1));
      assert.isTrue(ContactService.stringContainsWords(string, words2));
      assert.isFalse(ContactService.stringContainsWords(string, words3));
    });

  });

});
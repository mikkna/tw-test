import qs from 'qs';
import Contact from '../entities/contact';

export default class ContactService {
  constructor({ http }) {
    this.http = http;
    this.endpoint = '/contacts';
  }

  /**
   *
   * @param limit
   * @param query
   * @param offset
   * @returns {Promise<Contact[]>}
   */
  async get({ limit, query = '', offset } = {}) {
    const params = {
      limit,
      offset,
    };
    if (query.length) {
      params.query = query;
    }

    const q = qs.stringify(params);

    const contacts = await this.http.get(this.endpoint + (q ? `?${q}` : '')).catch(() => []);
    return contacts.map(c => new Contact(c));
  }
}

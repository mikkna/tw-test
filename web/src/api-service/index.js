import axios from 'axios';
import ContactService from './contact';

const API_URL = 'http://localhost:3000/api/v1';

export default function (app) {
  const axiosInstance = axios.create({
    baseURL: API_URL,
  });

  // simplify getting data from an API response
  const http = {
    get(url) {
      return axiosInstance.get(url).then(res => res.data);
    },
  };

  app.prototype.services = {
    contact: new ContactService({ http }),
  };
}

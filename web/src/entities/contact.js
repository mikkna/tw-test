class Contact {
  constructor({
    id, name, phoneNumber, address, imageURL,
  }) {
    this.id = id;
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.address = address;
    this.imageURL = imageURL;
  }
}

export default Contact;

import Vue from 'vue';
import App from './App.vue';
import registerServices from './api-service';

registerServices(Vue);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');

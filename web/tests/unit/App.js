import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import App from '../../src/App.vue';
import Home from '../../src/views/Home.vue';

describe('App.vue', () => {
  it('renders Home', () => {
    const wrapper = shallowMount(App);
    expect(wrapper.findAll(Home).length).to.equal(1);
  });
});

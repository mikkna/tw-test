import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Contact from '../../src/components/Contact.vue';

const contact = {
  imageURL: 'test',
  id: 1,
  name: 'name1',
  address: 'address',
  phoneNumber: 123,
};

describe('Contact.vue', () => {
  it('renders a contact\'s info', () => {
    const wrapper = shallowMount(Contact, {
      propsData: { contact },
    });
    expect(wrapper.html()).to.include(contact.imageURL);
    expect(wrapper.text()).to.include(contact.name);
    expect(wrapper.text()).to.include(contact.address);
    expect(wrapper.text()).to.include(contact.phoneNumber);
  });
});

import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import ContactList from '../../src/components/ContactList.vue';
import Contact from '../../src/components/Contact.vue';

const contacts = [
  {
    id: 1,
  }, {
    id: 2,
  },
];

describe('ContactList.vue', () => {
  it('renders list of contacts', () => {
    const wrapper = shallowMount(ContactList, {
      propsData: { contacts },
    });
    expect(wrapper.findAll(Contact).length).to.equal(contacts.length);
  });
});

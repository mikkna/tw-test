import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import Home from '../../src/views/Home.vue';
import ContactList from '../../src/components/ContactList.vue';

const contacts = [
  {
    id: 1,
  }, {
    id: 2,
  },
];

describe('Home.vue', () => {
  it('renders required elements', () => {
    Home.created = function () {};
    const wrapper = shallowMount(Home);
    wrapper.setData({ contacts });
    expect(wrapper.findAll(ContactList).length).to.equal(1);
    expect(wrapper.findAll('input[type=text]').length).to.equal(1);
  });

  it('getContacts passes on params correctly', () => {
    const params = { limit: 40, offset: 20, query: 'asd' };
    const context = {
      services: {
        contact: {
          get(args) {
            expect(args).to.deep.equal(params);
          },
        },
      },
    };
    Home.methods.getContacts.call(context, params);
  });

  it('loadMoreContacts works like a charm', async () => {
    const context = {
      filteredContacts() {
        return [1, 2];
      },
      async getContacts() {
        return [1, 2];
      },
      contacts: [],
    };
    await Home.methods.loadMoreContacts.call(context, { replace: true });
    expect(context.contacts, 'replace: true').to.deep.equal([1, 2]);
    await Home.methods.loadMoreContacts.call(context, { replace: false });
    expect(context.contacts, 'replace: false').to.deep.equal([1, 2, 1, 2]);
  });
});

import { expect } from 'chai';
import qs from 'qs';
import register from '../../src/api-service';
import ContactService from '../../src/api-service/contact';
import Contact from '../../src/entities/contact';

describe('api-service', () => {
  it('attaches services to Vue instance', () => {
    const obj = {
      prototype: {},
    };
    register(obj);
    expect(obj.prototype.services).to.be.a('object');
    expect(obj.prototype.services.contact).to.be.a('object');
  });
});

const contacts = [
  {
    id: 1,
  }, {
    id: 2,
  },
];

describe('contact service', () => {
  const http = {
    async get(args) {
      return args;
    },
  };

  it('has correct properties', () => {
    const contactService = new ContactService({ http });
    expect(contactService.endpoint, 'endpoint').to.be.a('string');
    expect(contactService.get, 'get').to.be.a('function');
  });

  it('get works like a charm', async () => {
    const contactService = new ContactService({ http });

    const params = { limit: 40, offset: 20, query: 'asd' };
    http.get = async function (args) {
      expect(args, 'parameters are passed correctly to api').to.include(qs.stringify(params));
      return contacts;
    };
    const queriedContacts = await contactService.get(params);
    expect(queriedContacts, 'correct number of contacts is returned').to.have.lengthOf(contacts.length);
    expect(queriedContacts[0]).to.be.instanceOf(Contact);
  });
});
